// importing mangoose from mangoose
import mongoose from "mongoose";

/*
    creating a schema for user_register_data
    in that withrespected datatypes
*/
const USER_REGISTER_SCHEMA = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  //re_enter_password: String,
  email: String,
  phone_number: Number,
  security_question: String,
});

/*
    creating a schema for admin_register_data
    in that withrespected datatypes
*/
const ADMIN_REGISTER_SCHEMA = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  //re_enter_password: String,
  email: String,
  phone_number: Number,
  security_question: String,
});

/* 
    creating a schema for the user_login_data
    withrespected datatypes
*/
const USER_LOGIN_SCHEMA = new mongoose.Schema({
  username: String,
  password: String,
  jwt_token: String,
  logon_time: Date,
});

/* 
    creating a schema for the admin_login_data
    withrespected datatypes
*/
const ADMIN_LOGIN_SCHEMA = new mongoose.Schema({
  username: String,
  password: String,
  jwt_token: String,
  logon_time: Date,
});

/**
 *  creating Schema for posting restaurants by the admin
 * creating the model for restaurant
 */
const ArtistDetails = new mongoose.Schema({
  name: String,
  image_url: String,
  monthly_listeners: Number,
});

/**
 * Schema for by foodItems of the restaurants
 * creating the model foodItem
 */
const PopularSongs = new mongoose.Schema({
  song_name: String,
  image_url: String,
  is_check:Boolean,
  listener_count: Number,
});

/**
 * creating Schema for user rating
 * creating the model for user rating
 * 
 */
const UserRatingSchema = new mongoose.Schema({
  rating: Number,
  rating_text: String,
  rater: { type: mongoose.Schema.Types.ObjectId, ref: "User" }, //UUID of 'user_register'
  ratee: { type: mongoose.Schema.Types.ObjectId, ref: "Restaurant" }, //UUID of 'restaurant_data'
});
// exporting the USER_REGISTER_SCHEMA, USER_LOGIN_DATA schemas from the schema.js file
export {
  USER_REGISTER_SCHEMA,
  USER_LOGIN_SCHEMA,
  ADMIN_REGISTER_SCHEMA,
  UserRatingSchema,
  ADMIN_LOGIN_SCHEMA,
  ArtistDetails,
  PopularSongs
};
